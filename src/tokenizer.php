<?php
$filename = isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : '';

if ($filename === '') {
    echo 'Invalid filename';
    exit(1);
}
if (!is_readable($filename)) {
    echo 'Can\'t read filename';
    exit(1);
}

$source = file_get_contents($filename);
$tokens = token_get_all($source);

$inWhile = true;
foreach ($tokens as $token) {
    if (is_string($token)) {
        // simple 1-character token
        echo "\nCHAR\n";
        echo $token;
    } else {
        // token array
        list($id, $text) = $token;

        echo "\nTOKEN : ".token_name($id)."\n";
        switch ($id) {
            case T_WHILE:
                $inWhile = true;
                echo 'WHILE';
                break;

            case T_COMMENT:
            case T_DOC_COMMENT:
                // no action on comments
                break;

            default:
                // anything else -> output "as is"
                echo $text;
                break;
        }
    }
}
